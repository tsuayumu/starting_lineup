class Lineup < ActiveRecord::Base
	class << self
	    def search(query)
	      rel = order("id")
	      if query.present?
	        rel = rel.where("team LIKE ? ",
	          "%#{query}%")
	      end
	      rel
	    end
	end
end
