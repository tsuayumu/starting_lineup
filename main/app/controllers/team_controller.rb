class TeamController < ApplicationController
	def member
		case params[:team]
		when "Giants" then
			@member = ["読売ジャイアンツ","立岡","坂本","長野","大田","亀井","クルーズ","阿部","岡本","菅野"]
			@lineups = Lineup.order("nickname")
		when "Carp" then
			@member = ["広島東洋カープ","田中","坂本","長野","大田","亀井","クルーズ","阿部","岡本","菅野"]
		else 
			@member = ["阪神タイガース","高山","坂本","長野","大田","亀井","クルーズ","阿部","岡本","菅野"]
		end
	end
end
