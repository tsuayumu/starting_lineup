class LineupsController < ApplicationController
	def index
		@lineups = Lineup.order("id")
	end

	def show
		@lineup = Lineup.find(params[:id])
	end

	def new
		@lineup = Lineup.new(nickname: "ayumu")
	end

	def edit
		@lineup = Lineup.find(params[:id])
	end

	def create
	end

	def update
	end

	def destroy
	end

	def search
		@lineups = Lineup.search(params[:q])
		render "index"
	end
end
