nicknames = %w(Taro Jiro Hana John Mike Sophy Bill Alex Mary Tom)
0.upto(9) do |idx|
  Lineup.create(
    nickname: "#{nicknames[idx]}",
    one: "#{nicknames[idx % 4]}",
    two: "#{nicknames[idx % 4]}",
    three: "#{nicknames[idx % 4]}",
    four: "#{nicknames[idx % 4]}",
    five: "#{nicknames[idx % 4]}",
    six: "#{nicknames[idx % 4]}",
    seven: "#{nicknames[idx % 4]}",
    eight: "#{nicknames[idx % 4]}",
    nine: "#{nicknames[idx % 4]}",
    coment: "#{nicknames[idx % 4]}",
    team: "Giants"
  )
end
