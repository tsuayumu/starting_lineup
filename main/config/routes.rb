Rails.application.routes.draw do
  root "top#index"
  get "team/:team" => "team#member"

  resources :lineups do
  	collection { get "search" }
  end
end
