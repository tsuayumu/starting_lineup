	<section id="footer">
		<h2 class="">Site Map</h2>
		<ul class="footer-work">
            <p class="mt0">セリーグ</p>
            <li><a href="Swallows.php">東京ヤクルトスワローズ</a></li>
            <li><a href="Giants.php">読売巨人ジャイアンツ</a></li>
            <li><a href="Tigers.php">阪神タイガーズ</a></li>
            <li><a href="Carp.php">広島東洋カープ</a></li>
            <li><a href="Dragons.php">中日ドラゴンズ</a></li>
            <li><a href="Baystars.php">DeNA横浜ベイスターズ</a></li>
        </ul>
        <ul class="footer-work">
            <p class="mt0">パリーグ</p>
            <li><a href="Hawks.php">福岡ソフトバンクホークス</a></li>
            <li><a href="Fighters.php">北海道日本ハムファイターズ</a></li>
            <li><a href="Marines.php">千葉ロッテマリーンズ</a></li>
            <li><a href="Lions.php">西武ライオンズ</a></li>
            <li><a href="Buffaloes.php">オリックスバファローズ</li>
            <li><a href="Eagles.php">東北楽天イーグルズ</a></li>
		</ul>
    </section>

    </body>
</html>