<?php

//ユーザがスタメンを投稿したものを追加する
function add_lineup ($team) {
	require('dbconnect.php');
	$user = trim($_POST['user']);
	$one = trim($_POST['one']);
	$two = trim($_POST['two']);
	$three = trim($_POST['three']);
	$four = trim($_POST['four']);
	$five = trim($_POST['five']);
	$six = trim($_POST['six']);
	$seven = trim($_POST['seven']);
	$eight = trim($_POST['eight']);
	$nine = trim($_POST['nine']); 

	$sql = sprintf('INSERT INTO %s SET user="%s",one="%s",two="%s",three="%s",four="%s",five="%s",six="%s",seven="%s",eight="%s",nine="%s"',//SQLの作成
		mysqli_real_escape_string($db, $team),
		mysqli_real_escape_string($db, $user),//無害化処置
		mysqli_real_escape_string($db, $one),
		mysqli_real_escape_string($db, $two),
		mysqli_real_escape_string($db, $three),
		mysqli_real_escape_string($db, $four),
		mysqli_real_escape_string($db, $five),
		mysqli_real_escape_string($db, $six),
		mysqli_real_escape_string($db, $seven),
		mysqli_real_escape_string($db, $eight),
		mysqli_real_escape_string($db, $nine)
	);

	mysqli_query($db, $sql);
}

//ユーザのコメントを追加する
function add_coment($team) {
	require('dbconnect.php');
	$user = trim($_POST['name']);
	$message = trim($_POST['message']);
	$id = trim($_POST['id']);

	$user = ($user === '') ? '名無しさん' : $user;
	$postedat = date('Y-m-d H:i:s');
	$message = str_replace("\t",' ',$message);
	$user = str_replace("\t",' ',$user);
	$newdata =$message . "\t" . $postedat . "\t" . $user . "\n";

	$sql = sprintf('SELECT * FROM %s WHERE id = "%s"',
		mysqli_real_escape_string($db, $team), 
		mysqli_real_escape_string($db, $id)
	);	

	$oldresult = mysqli_query($db, $sql);

	$oldresult = $oldresult->fetch_array(MYSQLI_ASSOC);

	$newdata = $message . "\t" . $postedat . "\t" . $user . "<br />" . $oldresult['coment'];
	
	$sql = sprintf('UPDATE %s SET coment="%s" WHERE id = "%s"', 
			mysqli_real_escape_string($db, $team),
			mysqli_real_escape_string($db, $newdata),
			mysqli_real_escape_string($db, $id)
			);

	mysqli_query($db, $sql);
}

?>