<!DOCTYPE html>
<html>
    <head>
        <title>プロ野球開幕スタメン</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
        <link rel="stylesheet" href="css/uikit.min.css" />
        <link rel="stylesheet" href="scss/style.css" />
        <link rel="stylesheet" href="css/components/autocomplete.css">
        <link id="data-uikit-theme" rel="stylesheet" href="css/uikit.docs.min.css">
        <link href='http://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/docs.css">
        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit.js"></script>
        <script src="js/components/sticky.js"></script>
        <script src="js/components/autocomplete.js"></script>
        <script src="js/components/lightbox.js"></script>

        <script>
        //googleアナリティクス
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-71664741-2', 'auto');
          ga('require', 'displayfeatures');
          ga('send', 'pageview');
        </script>
    </head>
    
    <body>

    <div id="pagetop">
    	<p class="uk-float-right" style="display: block; position: fixed; bottom: 0px; margin-top: 0px;">
    		<a href="#home-01" data-uk-smooth-scroll=""><img src="https://www.mymall.co.jp/cmscp/wp-content/themes/mymall/img/common/footer/pagetop.png" alt="ページの上部へ"></a>
    	</p>
    </div>

	<nav id="header" class="uk-navbar" data-uk-sticky>
        <ul class="uk-navbar-nav">
            <li><a href="index.php"><span class="logo">プロ野球開幕スタメン</span></a></li>
        </ul>
        <div class="uk-navbar-flip">
            <ul class="uk-navbar-nav hidden-ss">
                <li><a href="index.php">home</a></li>
                <li><a href="Swallows.php"><img src="images/home/01_S.jpg" alt="東京ヤクルトスワローズ" style="height: 40px;"></a></li>
                <li><a href="Giants.php"><img src="images/home/02_S.jpg" alt="読売巨人ジャイアンツ" style="height: 40px;"></a></li>
                <li><a href="Tigers.php"><img src="images/home/03_S.jpg" alt="阪神タイガーズ" style="height: 40px;"></a></li>
                <li><a href="Carp.php"><img src="images/home/04_S.jpg" alt="広島東洋カープ" style="height: 40px;"></a></li>
                <li><a href="Dragons.php"><img src="images/home/05_S.jpg" alt="中日ドラゴンズ" style="height: 40px;"></a></li>
                <li><a href="Baystars.php"><img src="images/home/06_S.jpg" alt="DeNA横浜ベイスターズ" style="height: 40px;"></a></li>
                <li><a href="Hawks.php"><img src="images/home/01_P.jpg" alt="福岡ソフトバンクホークス" style="height: 40px;"></a></li>
                <li><a href="Fighters.php"><img src="images/home/02_P.jpg" alt="北海道日本ハムファイターズ" style="height: 40px;"></a></li>
                <li><a href="Marines.php"><img src="images/home/03_P.jpg" alt="千葉ロッテマリーンズ" style="height: 40px;"></a></li>
                <li><a href="Lions.php"><img src="images/home/04_P.jpg" alt="西武ライオンズ" style="height: 40px;"></a></li>
                <li><a href="Buffaloes.php"><img src="images/home/05_P.jpg" alt="オリックスバファローズ" style="height: 40px;"></a></li>
                <li><a href="Eagles.php"><img src="images/home/06_P.jpg" alt="東北楽天イーグルズ" style="height: 40px;"></a></li>
            </ul>
            <div class="uk-button-dropdown visible-ss" data-uk-dropdown>
                <button class="uk-button">Hover me <i class="uk-icon-caret-down"></i></button>
                <div class="uk-dropdown">
                    <ul class="uk-nav uk-nav-dropdown">
                        <li><a href="index.html">home</a></li>
                        <div class="uk-grid uk-grid-width-1-2">
                            <li><a href="Swallows.php"><img src="images/home/01_S.jpg" alt="東京ヤクルトスワローズ" ></a></li>
                            <li><a href="Giants.php"><img src="images/home/02_S.jpg" alt="読売巨人ジャイアンツ" ></a></li>
                            <li><a href="Tigers.php"><img src="images/home/03_S.jpg" alt="阪神タイガーズ" ></a></li>
                            <li><a href="Carp.php"><img src="images/home/04_S.jpg" alt="広島東洋カープ" ></a></li>
                            <li><a href="Dragons.php"><img src="images/home/05_S.jpg" alt="中日ドラゴンズ" ></a></li>
                            <li><a href="Baystars.php"><img src="images/home/06_S.jpg" alt="DeNA横浜ベイスターズ" ></a></li>
                            <li><a href="Hawks.php"><img src="images/home/01_P.jpg" alt="福岡ソフトバンクホークス" ></a></li>
                            <li><a href="Fighters.php"><img src="images/home/02_P.jpg" alt="北海道日本ハムファイターズ" ></a></li>
                            <li><a href="Marines.php"><img src="images/home/03_P.jpg" alt="千葉ロッテマリーンズ" ></a></li>
                            <li><a href="Lions.php"><img src="images/home/04_P.jpg" alt="西武ライオンズ" ></a></li>
                            <li><a href="Buffaloes.php"><img src="images/home/05_P.jpg" alt="オリックスバファローズ" ></a></li>
                            <li><a href="Eagles.php"><img src="images/home/06_P.jpg" alt="東北楽天イーグルズ" ></a></li>
                        </div>                
                    </ul>
                </div>
            </div>
        </div>
    </nav>