<?php
	
	if ($team == "Giants") {
		$teamName = "読売ジャイアンツ";
		$default1 = "立岡";
		$default2 = "坂本";
		$default3 = "長野";
		$default4 = "大田";
		$default5 = "亀井";
		$default6 = "クルーズ";
		$default7 = "阿部";
		$default8 = "岡本";
		$default9 = "菅野";
	}
	else if($team == "Swallows") {
		$teamName = "東京ヤクルトスワローズ";
		$default1 = "川端";
		$default2 = "上田";
		$default3 = "山田";
		$default4 = "雄平";
		$default5 = "バレンティン";
		$default6 = "森岡";
		$default7 = "阿部";
		$default8 = "中村";
		$default9 = "石川";
	}
	else if($team == "Tigers") {
		$teamName = "阪神タイガーズ";
		$default1 = "鳥谷";
		$default2 = "大和";
		$default3 = "福留";
		$default4 = "ゴメス";
		$default5 = "新井";
		$default6 = "荒木";
		$default7 = "西岡";
		$default8 = "梅野";
		$default9 = "藤浪";
	}
	else if($team == "Dragons") {
		$teamName = "中日ドラゴンズ";
		$default1 = "大島";
		$default2 = "亀沢";
		$default3 = "森野";
		$default4 = "高橋";
		$default5 = "エルナンデス";
		$default6 = "荒木";
		$default7 = "遠藤";
		$default8 = "松井";
		$default9 = "大野";
	}
	else if($team == "Carp") {
		$teamName = "広島東洋カープ";
		$default1 = "野間";
		$default2 = "菊池";
		$default3 = "丸";
		$default4 = "新井";
		$default5 = "田中";
		$default6 = "梵";
		$default7 = "鈴木";
		$default8 = "會澤";
		$default9 = "黒田";
	}
	else if($team == "Baystars") {
		$teamName = "横浜DeNAベイスターズ";
		$default1 = "荒波";
		$default2 = "倉本";
		$default3 = "梶谷";
		$default4 = "筒香";
		$default5 = "ロペス";
		$default6 = "バルディリス";
		$default7 = "山崎";
		$default8 = "高城";
		$default9 = "三浦";
	}
	else if($team == "Hawks") {
		$teamName = "福岡ソフトバンクホークス";
		$default1 = "中村";
		$default2 = "今宮";
		$default3 = "長谷川";
		$default4 = "柳田";
		$default5 = "松田";
		$default6 = "内川";
		$default7 = "上林";
		$default8 = "細川";
		$default9 = "本多";
	}
	else if($team == "Fighters") {
		$teamName = "日本ハムファイターズ";
		$default1 = "中島";
		$default2 = "田中";
		$default3 = "陽";
		$default4 = "中田";
		$default5 = "近藤";
		$default6 = "レアード";
		$default7 = "大谷";
		$default8 = "大野";
		$default9 = "西川";
	}
	else if($team == "Marines") {
		$teamName = "千葉ロッテマリーンズ";
		$default1 = "鈴木";
		$default2 = "萩野";
		$default3 = "清田";
		$default4 = "井口";
		$default5 = "角中";
		$default6 = "デスパイネ";
		$default7 = "中村";
		$default8 = "田村";
		$default9 = "加藤";
	}
	else if($team == "Lions") {
		$teamName = "西武ライオンズ";
		$default1 = "秋山";
		$default2 = "栗山";
		$default3 = "浅村";
		$default4 = "中村";
		$default5 = "メヒア";
		$default6 = "森";
		$default7 = "大崎";
		$default8 = "炭谷";
		$default9 = "熊代";
	}
	else if($team == "Buffaloes") {
		$teamName = "オリックスバファローズ";
		$default1 = "糸井";
		$default2 = "西野";
		$default3 = "小谷野";
		$default4 = "岡田";
		$default5 = "中島";
		$default6 = "カラバイヨ";
		$default7 = "ブランコ";
		$default8 = "伊藤";
		$default9 = "安達";
	}
	else if($team == "Eagles") {
		$teamName = "東北楽天ゴールデンイーグルズ";
		$default1 = "岡島";
		$default2 = "藤田";
		$default3 = "銀次";
		$default4 = "中川";
		$default5 = "ペーニャ";
		$default6 = "後藤";
		$default7 = "西田";
		$default8 = "嶋";
		$default9 = "松井";
	}
	

?>