<div id="<?php echo $team; ?>">

<div class="uk-container uk-container-center">

	<h1 class="page-tit white"><?php echo $teamName; ?> 2016開幕スタメン予想</h1>

	<div class="form-content">
		<form action="" method="post" onclick="ga('send','event','Team','Post','<?php echo $teamName; ?>',1,{'nonInteraction':ture})">
			<h2 class="white">打順入力</h2>
			<div class="block">1番: <input type="text" name="one" placeholder="入力してください" required="required" value="<?php echo $default1; ?>"></div>
			<div class="block">2番: <input type="text" name="two" placeholder="入力してください" required="required" value="<?php echo $default2; ?>"></div>
			<div class="block">3番: <input type="text" name="three" placeholder="入力してください" required="required" value="<?php echo $default3; ?>"></div>
			<div class="block">4番: <input type="text" name="four" placeholder="入力してください" required="required" value="<?php echo $default4; ?>"></div>
			<div class="block">5番: <input type="text" name="five" placeholder="入力してください" required="required" value="<?php echo $default5; ?>"></div>
			<div class="block">6番: <input type="text" name="six" placeholder="入力してください" required="required" value="<?php echo $default6; ?>"></div>
			<div class="block">7番: <input type="text" name="seven" placeholder="入力してください" required="required" value="<?php echo $default7; ?>"></div>
			<div class="block">8番: <input type="text" name="eight" placeholder="入力してください" required="required" value="<?php echo $default8; ?>"></div>
			<div class="block">9番: <input type="text" name="nine" placeholder="入力してください" required="required" value="<?php echo $default9; ?>"></div>
			user: <input type="text" name="user" value="名無し">
			<input type="submit" value="投稿">
			<p>＊デフォルトは管理人予想</p>
		</form>
	</div>

	<?php
			$row = $result->num_rows;
	?>

	<h2 class="white">投稿一覧<?php echo $row;?>件</h2>

	<div class="hidden-ss">
		<?php 
			foreach ( $result as $results):
		?>
			<div id="<?php $tmp = $results['id'] * 10; echo $tmp;?>" class="bbs-content uk-grid">
				<section class="uk-width-2-5 member" style="text-align: center;">
					<div class="back-black">
						予想者：<?php echo $results['user']; ?>
						<p class="mt0">1番  　<?php echo $results['one']; ?></p>
						<p class="mt0">2番  　<?php echo $results['two']; ?></p>
						<p class="mt0">3番  　<?php echo $results['three']; ?></p>
						<p class="mt0">4番  　<?php echo $results['four']; ?></p>
						<p class="mt0">5番  　<?php echo $results['five']; ?></p>
						<p class="mt0">6番  　<?php echo $results['six']; ?></p>
						<p class="mt0">7番  　<?php echo $results['seven']; ?></p>
						<p class="mt0">8番  　<?php echo $results['eight']; ?></p>
						<p class="mt0">9番  　<?php echo $results['nine']; ?></p>
					</div>
				</section>
				<section class="uk-width-3-5 coment">
					<h3 class="">コメント</h3>
					<form action="" method="post" onclick="ga('send','event','Comment','Post','<?php echo $teamName; ?>',1,{'nonInteraction':ture})">
						user: <input type="text" name="name" size="10" value="名無し">
						message: <input type="text" name="message">
						<input type="hidden" name="id" value=" <?php echo $results['id']; ?>" >
						<input type="hidden" name="device" value="lg" >
						<input type="submit" value="投稿">
					</form>
					<div class="text-coment" style="">
						<?php echo $results['coment']; ?>
					</div>
				</section>
			</div>
		<?php endforeach; ?> 
	</div>

	<div class="visible-ss">
		<?php 
			foreach ($result as $results) : 
		?>
			<div id="<?php echo $results['id'];?>" class="bbs-content uk-container uk-container-center">
				<section class="member" style="text-align: center;">
					予想者：<?php echo $results['user']; ?>
					<p class="mt0">1番  　<?php echo $results['one']; ?></p>
					<p class="mt0">2番  　<?php echo $results['two']; ?></p>
					<p class="mt0">3番  　<?php echo $results['three']; ?></p>
					<p class="mt0">4番  　<?php echo $results['four']; ?></p>
					<p class="mt0">5番  　<?php echo $results['five']; ?></p>
					<p class="mt0">6番  　<?php echo $results['six']; ?></p>
					<p class="mt0">7番  　<?php echo $results['seven']; ?></p>
					<p class="mt0">8番  　<?php echo $results['eight']; ?></p>
					<p class="mt0">9番  　<?php echo $results['nine']; ?></p>
				</section>
				<section class="coment">
					<h3 class="" style="padding:10px 0 0 0;">コメント</h3>
					<form action="" method="post" onclick="ga('send','event','Comment','Post','<?php echo $teamName; ?>',1,{'nonInteraction':ture})">
						user: <input type="text" name="name" size="10" value="名無し">
						<br>
						message: <input type="text" name="message">
						<input type="hidden" name="id" value=" <?php echo $results['id']; ?>" >
						<input type="hidden" name="device" value="ss" >
						<input type="submit" value="投稿">
					</form>
					<div class="text-coment">
						<?php echo $results['coment']; ?>
					</div>
				</section>
			</div>
		<?php endforeach; ?> 
	</div>

</div>

</div>