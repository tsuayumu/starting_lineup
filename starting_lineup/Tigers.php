<?php

function h($s) {
	return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
}

$team = basename($_SERVER['PHP_SELF'],".php");

require('php-tmplate/dbconnect.php');//データベースへの接続
require('php-tmplate/add.php'); //追加する関数
require('php-tmplate/team.php'); //チームの情報

if (isset($_POST['one']) && isset($_POST['two'])&& isset($_POST['three'])&& isset($_POST['four'])&& isset($_POST['five'])&& isset($_POST['six'])&& isset($_POST['seven'])&& isset($_POST['eight']) && isset($_POST['nine']) && isset($_POST['user'])) {
	add_lineup($team);
}
$sql = sprintf('SELECT * FROM %s ORDER BY id DESC',mysqli_real_escape_string($db, $team));

$result = mysqli_query($db, $sql);


//コメント欄
if (isset($_POST['name']) && isset($_POST['message'])) {

	add_coment($team); //コメント追加
	
	$device = trim($_POST['device']);
	$id = trim($_POST['id']);

	if ($device == "ss") {
		header('Location: '. $team . '.php#' . $id);
	}
	else {
		$id = $id * 10 + 10;
		header('Location: '. $team . '.php#' . $id);
	}
	
}
?>




<?php

	include 'php-tmplate/header.php';

	include 'php-tmplate/team-content.php';

	include 'php-tmplate/footer.php';

?>
