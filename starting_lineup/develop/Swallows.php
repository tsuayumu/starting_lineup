<?php

function h($s) {
	return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
}

$datafile = 'bbs.dat';

require('dbconnect.php');//データベースへの接続


if (isset($_POST['one']) && isset($_POST['two'])&& isset($_POST['three'])&& isset($_POST['four'])&& isset($_POST['five'])&& isset($_POST['six'])&& isset($_POST['seven'])&& isset($_POST['eight']) && isset($_POST['nine']) && isset($_POST['user'])) {
	$user = trim($_POST['user']);
	$one = trim($_POST['one']);
	$two = trim($_POST['two']);
	$three = trim($_POST['three']);
	$four = trim($_POST['four']);
	$five = trim($_POST['five']);
	$six = trim($_POST['six']);
	$seven = trim($_POST['seven']);
	$eight = trim($_POST['eight']);
	$nine = trim($_POST['nine']); 

	$sql = sprintf('INSERT INTO Swallows SET user="%s",one="%s",two="%s",three="%s",four="%s",five="%s",six="%s",seven="%s",eight="%s",nine="%s"',//SQLの作成
mysqli_real_escape_string($db, $user),//無害化処置
mysqli_real_escape_string($db, $one),
mysqli_real_escape_string($db, $two),
mysqli_real_escape_string($db, $three),
mysqli_real_escape_string($db, $four),
mysqli_real_escape_string($db, $five),
mysqli_real_escape_string($db, $six),
mysqli_real_escape_string($db, $seven),
mysqli_real_escape_string($db, $eight),
mysqli_real_escape_string($db, $nine)
);

	mysqli_query($db, $sql);
	
}

$sql = "SELECT * FROM Swallows";

$result = mysqli_query($db, $sql);


//コメント欄
if (isset($_POST['name']) && isset($_POST['message'])) {
	$user = trim($_POST['name']);
	$message = trim($_POST['message']);
	$id = trim($_POST['id']);

	if($message !== ''){
		$user = ($user === '') ? '名無しさん' : $user;
		$postedat = date('Y-m-d H:i:s');
		$message = str_replace("\t",' ',$message);
		$user = str_replace("\t",' ',$user);
		$newdata =$message . "\t" . $postedat . "\t" . $user . "\n";

		$sql = sprintf('SELECT * FROM Swallows WHERE id = "%s"', 
				mysqli_real_escape_string($db, $id)
				);	

		$oldresult = mysqli_query($db, $sql);

		foreach ($oldresult as $oldresults) : 
		echo $oldresults['coment'];
		$newdata =$message . "\t" . $postedat . "\t" . $user . "<br />" . $oldresults['coment'] . "\n";
		endforeach;

		$sql = sprintf('UPDATE Swallows SET coment="%s" WHERE id = "%s"', 
				mysqli_real_escape_string($db, $newdata),
				mysqli_real_escape_string($db, $id)
				);

		mysqli_query($db, $sql);

		header('Location: bbs.php');
	}
}

?>




<?php
	include "header.php";
?>

<div id="Swallows">

<div class="uk-container uk-container-center">

	<section id="Swallows-01">
		<h1 class="page-tit white">東京ヤクルトスワローズ 2016開幕スタメン予想</h1>

		<div class="form-content">
			<form action="" method="post">
				<h2 class="white">打順入力</h2>
				<div class="block">1番: <input type="text" name="one" placeholder="入力してください" required="required" value="川端"></div>
				<div class="block">2番: <input type="text" name="two" placeholder="入力してください" required="required" value="上田"></div>
				<div class="block">3番: <input type="text" name="three" placeholder="入力してください" required="required" value="山田"></div>
				<div class="block">4番: <input type="text" name="four" placeholder="入力してください" required="required" value="畠山"></div>
				<div class="block">5番: <input type="text" name="five" placeholder="入力してください" required="required" value="雄平"></div>
				<div class="block">6番: <input type="text" name="six" placeholder="入力してください" required="required" value="バレンティン"></div>
				<div class="block">7番: <input type="text" name="seven" placeholder="入力してください" required="required" value="森岡"></div>
				<div class="block">8番: <input type="text" name="eight" placeholder="入力してください" required="required" value="中村"></div>
				<div class="block">9番: <input type="text" name="nine" placeholder="入力してください" required="required" value="石川"></div>
				user: <input type="text" name="user" value="管理人">
				<input type="submit" value="投稿">
				<p>＊デフォルトは管理人予想</p>
			</form>
		</div>
	</section>

	<?php
			$row = $result->num_rows;
	?>

	<h2 class="white">投稿一覧<?php echo $row;?>件</h2>

	<div class="hidden-ss">
		<?php 
			foreach ($result as $results) : 
		?>
			<div class="bbs-content uk-grid">
				<section class="uk-width-2-5 member" style="text-align: center;">
					<div class="back-black">
						予想者：<?php echo $results['user']; ?>
						<p class="mt0">1番  　<?php echo $results['one']; ?></p>
						<p class="mt0">2番  　<?php echo $results['two']; ?></p>
						<p class="mt0">3番  　<?php echo $results['three']; ?></p>
						<p class="mt0">4番  　<?php echo $results['four']; ?></p>
						<p class="mt0">5番  　<?php echo $results['five']; ?></p>
						<p class="mt0">6番  　<?php echo $results['six']; ?></p>
						<p class="mt0">7番  　<?php echo $results['seven']; ?></p>
						<p class="mt0">8番  　<?php echo $results['eight']; ?></p>
						<p class="mt0">9番  　<?php echo $results['nine']; ?></p>
					</div>
				</section>
				<section class="uk-width-3-5 coment">
					<h3 class="">コメント</h3>
					<form action="" method="post">
						user: <input type="text" name="name" size="10">
						message: <input type="text" name="message">
						<input type="hidden" name="id" value=" <?php echo $results['id']; ?>" >
						<input type="submit" value="投稿">
					</form>
					<div class="text-coment" style="">
						<?php echo $results['coment']; ?>
					</div>
				</section>
			</div>
		<?php endforeach; ?> 
	</div>

	<div class="visible-ss">
		<?php 
			foreach ($result as $results) : 
		?>
		<div style="height: 100vh;">
			<div class="bbs-content uk-container uk-container-center">
				<section class="member" style="text-align: center;">
					予想者：<?php echo $results['user']; ?>
					<p class="mt0">1番  　<?php echo $results['one']; ?></p>
					<p class="mt0">2番  　<?php echo $results['two']; ?></p>
					<p class="mt0">3番  　<?php echo $results['three']; ?></p>
					<p class="mt0">4番  　<?php echo $results['four']; ?></p>
					<p class="mt0">5番  　<?php echo $results['five']; ?></p>
					<p class="mt0">6番  　<?php echo $results['six']; ?></p>
					<p class="mt0">7番  　<?php echo $results['seven']; ?></p>
					<p class="mt0">8番  　<?php echo $results['eight']; ?></p>
					<p class="mt0">9番  　<?php echo $results['nine']; ?></p>
				</section>
				<section class="coment">
					<h3 class="" style="padding:10px 0 0 0;">コメント</h3>
					<form action="" method="post">
						user: <input type="text" name="name" size="10">
						<br>
						message: <input type="text" name="message">
						<input type="hidden" name="id" value=" <?php echo $results['id']; ?>" >
						<input type="submit" value="投稿">
					</form>
					<div class="text-coment">
						<?php echo $results['coment']; ?>
					</div>
				</section>
			</div>
		</div>
		<?php endforeach; ?> 
	</div>

</div>

</div>

<?php

	include 'footer.php';

?>